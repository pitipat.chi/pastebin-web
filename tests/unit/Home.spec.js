import { mount, createLocalVue } from '@vue/test-utils';
import VueRouter from 'vue-router';
import Vuetify from 'vuetify';
import Home from '@/views/Home.vue';
import Paste from '@/views/Paste.vue';


jest.mock('@/api', () => {
  const error1 = { data: 'Either title or content are too long.' };
  const error2 = { data: 'title should not be blank' };
  return {
    createPaste: jest.fn((dao) => {
      if (dao.title === 'a'.repeat(513) || dao.content === 'a'.repeat(2 ** 16)) {
        return Promise.reject(error1);
      } if (dao.title === '') {
        return Promise.reject(error2);
      }
      return Promise.resolve({ data: { id: 1 } });
    }),
    getById: jest.fn((id) => {
      if (id === 1) {
        return Promise.resolve({ data: { title: 'title', content: 'content', createdAt: '1998-08-14 09:00:00' } });
      }
      return Promise.reject();
    }),
    recents: jest.fn(() => Promise.resolve([])),
  };
});


describe('Home.vue', () => {
  let wrapper;

  const routes = [
    { path: 'home', name: 'home', component: Home },
    { path: '/:id', name: 'paste', component: Paste },
  ];

  const vuetify = new Vuetify();
  const router = new VueRouter({ routes });
  const localVue = createLocalVue();
  localVue.use(Vuetify);
  localVue.use(VueRouter);

  it('test long title', () => {
    wrapper = mount(Home, {
      localVue,
      vuetify,
      router,
    });
    wrapper.setData({ dao: { title: 'a'.repeat(513), content: 'content' } });
    wrapper.find('button').trigger('click');
    expect(wrapper.vm.dialog).toBe(true);
  });

  it('test long content', () => {
    wrapper = mount(Home, {
      localVue,
      vuetify,
      router,
    });
    wrapper.setData({ dao: { title: 'title', content: 'a'.repeat(2 ** 16) } });
    wrapper.find('button').trigger('click');
    expect(wrapper.vm.dialog).toBe(true);
  });

  it('test blank title', () => {
    wrapper = mount(Home, {
      localVue,
      vuetify,
      router,
    });
    wrapper.setData({ dao: { title: '', content: 'long' } });
    wrapper.find('button').trigger('click');
    expect(wrapper.vm.dialog).toBe(true);
  });

  it('test paste successfully', () => {
    wrapper = mount(Home, {
      localVue,
      vuetify,
      router,
    });
    wrapper.setData({ dao: { title: 'title', content: 'content' } });
    wrapper.find('button').trigger('click');
    expect(wrapper.vm.dialog).toBe(false);
    wrapper.vm.$nextTick(() => {
      expect(wrapper.vm.id).toBe(1);
    });
  });
});
