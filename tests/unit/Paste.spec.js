import { mount, createLocalVue } from '@vue/test-utils';
import VueRouter from 'vue-router';
import Vuetify from 'vuetify';
import Home from '@/views/Home.vue';
import Paste from '@/views/Paste.vue';


jest.mock('@/api', () => {
  const error1 = { data: 'Either title or content are too long.' };
  const error2 = { data: 'title should not be blank' };
  return {
    createPaste: jest.fn((dao) => {
      if (dao.title === 'a'.repeat(513) || dao.content === 'a'.repeat(2 ** 16)) {
        return Promise.reject(error1);
      } if (dao.title === '') {
        return Promise.reject(error2);
      }
      return Promise.resolve({ data: { id: 1 } });
    }),
    getById: jest.fn((id) => {
      if (id === 1) {
        return Promise.resolve({ data: { title: 'title', content: 'content', createdAt: '1998-08-14 09:00:00' } });
      }
      return Promise.reject();
    }),
    recents: jest.fn(() => Promise.resolve([])),
  };
});


describe('Paste.vue', () => {
  let wrapper;

  const $route1 = {
    path: '/1',
    params: {
      id: 1,
    },
  };

  const $route2 = {
    path: '/2',
    params: {
      id: 2,
    },
  };

  const routes = [
    { path: 'home', name: 'home', component: Home },
    { path: '/:id', name: 'paste', component: Paste },
  ];

  const vuetify = new Vuetify();
  const router = new VueRouter({ routes });
  const localVue = createLocalVue();
  localVue.use(Vuetify);
  localVue.use(VueRouter);

  it('test found paste', () => {
    wrapper = mount(Home, {
      localVue,
      vuetify,
      router,
      mocks: {
        $route1,
      },
    });
    wrapper.vm.$nextTick(() => {
      expect(wrapper.vm.dialog).toBe(false);
    });
  });

  it('test not found paste', () => {
    wrapper = mount(Home, {
      localVue,
      vuetify,
      router,
      mocks: {
        $route2,
      },
    });
    wrapper.vm.$nextTick(() => {
      expect(wrapper.vm.dialog).toBe(true);
    });
  });
});
