from locust import HttpLocust, TaskSet, task, constant

# def paste(l):
    # l.client.post("/paste", json={"title":"title", "content":"content"})

def fetch(l):
    l.client.get("/1")

# def recents(l):
    # l.client.post("/recents")

class UserBehavior(TaskSet):
    tasks = {fetch: 1}


class WebsiteUser(HttpLocust):
    task_set = UserBehavior
    wait_time = constant(1)


