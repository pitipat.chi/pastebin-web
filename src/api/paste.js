import api from './api';

export const createPaste = dao => api.post('/paste', dao);
export const getById = id => api.get(`/${id}`);
export const recents = () => api.post('/recents');
