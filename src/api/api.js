import axios from 'axios';

const url = process.env.PROD ? 'https://naewmamuang.dev/api' : '/api';

const api = axios.create({
  baseURL: url,
});

api.interceptors.response.use(response => response, error => Promise.reject(error.response));

export default api;
