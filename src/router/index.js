import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '../views/Home.vue';
import Paste from '../views/Paste.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
  },
  {
    path: '/:id',
    name: 'paste',
    component: Paste,
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
